import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/fenlei.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'fenlei',
    component: HomeView,
  },
  {
    path: '/zuofa',
    name: 'zuofa',
    component: () => import('../views/zuofa.vue'),
  },
  {
    path: '/xiangqing',
    name: 'xiangqing',
    component: () => import('../views/xiangqing.vue'),
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import('../views/administrators.vue'),
    //redirect意味着重定向,浏览器跳转/container
    redirect: '/admin/container',
    children: [{
      path: 'container',
      name: 'container',
      component: () => import('../views/container.vue')
    }, {
      path: 'table',
      name: 'table',
      component: () => import('../views/table.vue')
    }, {
      path: 'form',
      name: 'form',
      component: () => import('../views/form.vue')
    }]
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/register.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
